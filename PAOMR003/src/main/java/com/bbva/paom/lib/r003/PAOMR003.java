package com.bbva.paom.lib.r003;

import java.util.List;
import java.util.Map;

public interface PAOMR003 {

	void execute(List<Map<String, Object>> mapIn);

}

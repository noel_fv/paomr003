package com.bbva.paom.lib.r003;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;

import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/META-INF/spring/PAOMR003-app.xml",
		"classpath:/META-INF/spring/PAOMR003-app-test.xml", "classpath:/META-INF/spring/PAOMR003-arc.xml",
		"classpath:/META-INF/spring/PAOMR003-arc-test.xml" })
public class PAOMR003Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(PAOMR003Test.class);

	@Resource(name = "paomR003")
	private PAOMR003 paomR003;

	@Resource(name = "applicationConfigurationService")
	private ApplicationConfigurationService applicationConfigurationService;

	@Before
	public void setUp() throws Exception {
		ThreadContext.set(new Context());
		getObjectIntrospection();
	}

	private Object getObjectIntrospection() throws Exception {
		Object result = this.paomR003;
		if (this.paomR003 instanceof Advised) {
			Advised advised = (Advised) this.paomR003;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}

	@Test
	public void executeTest() {
		LOGGER.info("Executing the test...");
		assertTrue(true);
//		paomR003.execute(mapIn);
	}

}

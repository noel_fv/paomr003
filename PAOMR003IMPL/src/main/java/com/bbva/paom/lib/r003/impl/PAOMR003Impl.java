package com.bbva.paom.lib.r003.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PAOMR003Impl extends PAOMR003Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger("PAOMR003Impl");
	private static final String TRACE_PCLDR003_IMPL = "[PCLD][PAOMR003Impl] - %s";

	@Override
	public void execute(List<Map<String, Object>> mapIn) {
		LOGGER.info(String.format(TRACE_PCLDR003_IMPL, "execute START"));

		Map<String, Object> inputTest = new HashMap<String, Object>();
		inputTest.put("ID_PETICION", "pet1");
		inputTest.put("CANAL", "can1");
		inputTest.put("PROCESO_CODIGO", "proccod1");
		inputTest.put("PROCESO_TAREA", "proctar1");
		inputTest.put("PROCESO_IDENTIFICADOR", "prociden1");
		inputTest.put("SOLICITANTE_TIPOIDENTIFICADOR", "soltip1");
		inputTest.put("SOLICITANTE_NROIDENTIFICADOR", "solnroiden1");
		inputTest.put("FECHA", new Date());
		inputTest.put("JSON_PETICION", "hola");
		int updatedNumber = this.jdbcUtils.update("PAOM.insertTrazabilidadData", inputTest);
		LOGGER.info(String.format(TRACE_PCLDR003_IMPL, "Result: " + updatedNumber));
		LOGGER.info(String.format(TRACE_PCLDR003_IMPL, "execute END"));

	}

}
